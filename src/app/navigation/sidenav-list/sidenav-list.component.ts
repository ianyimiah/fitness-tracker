import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {
  @Output() closeSidenav = new EventEmitter<void>();
  isAuth = false;
  authSubscription: Subscription

  constructor(private authService: AuthService) { }

  ngOnInit() {
    let authChangeObserver = {
      next: (authStatus) => {
        this.isAuth = authStatus;
      }
    }
    this.authService.authChange.subscribe(authChangeObserver);
  }

  closeMatSidenav() {
    this.closeSidenav.emit();
  }

  onLogout() {
    this.authService.logout();
  }

}
