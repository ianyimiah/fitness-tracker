import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('sidenav') MatSidenav: MatSidenav;

  constructor(private authService: AuthService) { }

  onSidenavToggle() {
    this.MatSidenav.toggle();
  }

  onSidenavClose() {
    this.MatSidenav.close();
  }

  ngOnInit() {
    this.authService.initAuthListener();
  }
}
