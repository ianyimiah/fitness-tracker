// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB-_7YWHZ7YXAokj0kr0rj2GFVRjxUk2cY",
    authDomain: "fitness-tracker-c2303.firebaseapp.com",
    databaseURL: "https://fitness-tracker-c2303.firebaseio.com",
    projectId: "fitness-tracker-c2303",
    storageBucket: "fitness-tracker-c2303.appspot.com",
    messagingSenderId: "66874262720"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
