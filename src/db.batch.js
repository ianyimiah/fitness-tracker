const firebase = require("firebase");
const faker = require("faker");
// Required for side-effects
require("firebase/firestore");

firebase.initializeApp({
    apiKey: "AIzaSyB-_7YWHZ7YXAokj0kr0rj2GFVRjxUk2cY",
    authDomain: "fitness-tracker-c2303.firebaseapp.com",
    databaseURL: "https://fitness-tracker-c2303.firebaseio.com",
    projectId: "fitness-tracker-c2303",
});

var db = firebase.firestore();

db.collection("availableExercises").add({
    name: "Push ups",
    duration: 90,
    calories: 23
})
.then(function(docRef) {
    console.log("Document written with ID: ", docRef.id);
    return;
})
.catch(function(error) {
    console.error("Error adding document: ", error);
    return;
});